# Task for Almedia #

### Descripton  ###
Our application has an offer job that fetches offers by HTTP requests from different offer networks (providers). The response payload is different for each provider, but we transform and store all offers into one database table. Please provide an approach to validate and transform all response payloads into the same type of object by using NestJS DTOs, and decorators. For when validation does not pass for one offer, please skip the offer, print a warning, and continue with the other offers of the provider.

In the link below is our offers entity class and a couple of response payloads.

You can use the *.payload.ts files data as constant variables to not waste time on making requests.

The goal is to convert different types of payloads into one entity and to also validate the payload data. When checking the challenge code we will specifically look for well structured code that can scale and handle multiple (20+) offer providers in a clean way.

For starting at firs need to start mysql docker container

```
docker composer up -d
```
after need to start application 

```
npm start
```

I set typeorm sync  with entities. After starting application db will be migrated automatically.

I create example for 2 data sources. from example Almedia passed me, 
For more sources you need follow  these steps.

#### 1 ####

Create new Dto file for validation in 

src/offers/dto/ 

#### 2 ####

Create  source Interface in src/offers/interfaces

```javascript

export interface InewSourcePayload {
  offer_id: string;
  offer_name: string;
  offer_desc: string;
  call_to_action: string;
  disclaimer: string;
  offer_url: string;
  icon: string;
  instructions: string;
  offer_url_easy: string;
  image_url: string;
  platform: string;
  device: string;
}
```
#### 3 ####

Append to src/offers/helpers/offerExtracters new function 
extractor 
```javascript

export function ExtractNewOffer(RequestData: Array<InewSourcePayload>) {
  const offers = [];
  for (const data of RequestData) {
    offers.push({
      name: data.offer_name,
      slug: uuidv4(),
      description: data.offer_desc,
      requirements: data.call_to_action,
      thumbnail: data.image_url,
      isDesktop: data.platform == 'desktop',
      isAndroid: data.device !== 'iphone_ipad',
      isIos: data.device == 'iphone_ipad',
      offerUrlTemplate: data.offer_url,
      providerName: 'source1',
      externalOfferId: data.offer_id,
    });
  }
  return offers;
}
```


#### 4 ####

Add to constatns/validations array 

```javascript

newSource: {
  dto: newSource,
    data:"set data path in request body ",
    extract: 'extract function',
}
```

