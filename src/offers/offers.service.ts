import {
  BadRequestException,
  HttpException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import validation from './constatns/validation';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { InjectRepository } from '@nestjs/typeorm';
import { Offer } from './entities/offer.entyty';
import { Repository } from 'typeorm';
import { observeNotification } from 'rxjs/internal/Notification';

@Injectable()
export class OffersService {
  constructor(
    @InjectRepository(Offer)
    private readonly userRepository: Repository<Offer>,
  ) {}
  async extractData(source, requestData) {
    const rules = validation[source];
    if (!rules) throw new NotFoundException();
    const data = rules.data(requestData);
    const errors = await this.validateDTO(rules, data);
    if (errors.length) throw new BadRequestException(errors);
    const Offers = rules.extract(data);
    return this.userRepository.save(Offers);
  }

  private async validateDTO(rules, requestData) {
    if (!rules) throw new NotFoundException();
    const validationErrors = [];
    for (const data of requestData) {
      const dto = plainToClass(rules.dto, data);
      const errors = await validate(dto);
      if (errors.length > 0) {
        validationErrors.push(
          errors.map((error) => Object.values(error.constraints)),
        );
      }
    }
    return validationErrors;
  }
}
