import { IOffer } from '../interfaces/IOffer';
import { ISource1Payload } from '../interfaces/ISource1Payload';
import { ISource2Payload } from '../interfaces/ISource2Payload';
import { v4 as uuidv4 } from 'uuid';
import { Req } from '@nestjs/common';

export function ExtractOffer1(RequestData: Array<ISource1Payload>) {
  const offers = [];
  for (const data of RequestData) {
    offers.push({
      name: data.offer_name,
      slug: uuidv4(),
      description: data.offer_desc,
      requirements: data.call_to_action,
      thumbnail: data.image_url,
      isDesktop: data.platform == 'desktop',
      isAndroid: data.device !== 'iphone_ipad',
      isIos: data.device == 'iphone_ipad',
      offerUrlTemplate: data.offer_url,
      providerName: 'source1',
      externalOfferId: data.offer_id,
    });
  }
  return offers;
}
export function ExtractOffer2(RequestData: Array<ISource2Payload>) {
  const offers = [];
  for (const data of RequestData) {
    offers.push({
      name: data.name,
      slug: uuidv4(),
      description: data.description,
      requirements: data.instructions,
      thumbnail: data.icon,
      isDesktop: data.OS.web,
      isAndroid: data.OS.android,
      isIos: data.OS.ios,
      offerUrlTemplate: data.tracking_url,
      providerName: 'source2',
      externalOfferId: data.campaign_id,
    });
  }
  return offers;
}
