import Source1Dto from '../dto/Source1.dto';
import Source2Dto from '../dto/Source2.dto';
import { ISource1Payload } from '../interfaces/ISource1Payload';
import { ISource2Payload } from '../interfaces/ISource2Payload';
import { ExtractOffer1, ExtractOffer2 } from '../helpers/offerExtracters';

export default {
  source1: {
    dto: Source1Dto,
    data: (data): Array<ISource1Payload> => data?.response?.offers,
    extract: ExtractOffer1,
  },
  source2: {
    dto: Source2Dto,
    data: (data): Array<ISource2Payload> =>
      Object.values(data.data).map((item: any) => {
        return { ...item.Offer, OS: item.OS };
      }),
    extract: ExtractOffer2,
  },
};
