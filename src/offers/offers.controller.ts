import { Body, Controller, Headers, Post } from '@nestjs/common';
import { OffersService } from './offers.service';

@Controller('offers')
export class OffersController {
  constructor(private readonly offerService: OffersService) {}
  @Post('payload')
  async processPayload(@Body() offers, @Headers() headers) {
    return this.offerService.extractData(headers.host, offers);
  }
}
