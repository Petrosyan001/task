export interface ISource2Payload {
  campaign_id: string;
  name: string;
  description: string;
  instructions: string;
  tracking_url: string;
  icon: string;
  OS: {
    android: boolean;
    ios: boolean;
    web: boolean;
  };
}
