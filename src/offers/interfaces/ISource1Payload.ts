export interface ISource1Payload {
  offer_id: string;
  offer_name: string;
  offer_desc: string;
  call_to_action: string;
  disclaimer: string;
  offer_url: string;
  icon: string;
  instructions: string;
  offer_url_easy: string;
  image_url: string;
  platform: string;
  device: string;
}
