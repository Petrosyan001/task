import { IsNotEmpty, IsString, IsUrl, IsOptional } from 'class-validator';

export default class Source1Dto {
  @IsNotEmpty()
  @IsString()
  offer_id: string;

  @IsNotEmpty()
  @IsString()
  offer_name: string;

  @IsNotEmpty()
  @IsString()
  offer_desc: string;

  @IsNotEmpty()
  @IsString()
  call_to_action: string;

  @IsOptional()
  @IsString()
  disclaimer: string;

  @IsNotEmpty()
  @IsUrl()
  offer_url: string;

  @IsNotEmpty()
  @IsUrl()
  image_url: string;

  @IsNotEmpty()
  @IsString()
  platform: string;

  @IsNotEmpty()
  @IsString()
  device: string;
}
