// offer2.dto.ts
import { IsNotEmpty, IsString, IsUrl, IsBoolean } from 'class-validator';
import { Type } from 'class-transformer';

class OS {
  @IsBoolean()
  android: boolean;
  @IsBoolean()
  ios: boolean;
  @IsBoolean()
  web: boolean;
}
export default class Source2Dto {
  @IsNotEmpty()
  campaign_id: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  description: string;

  @IsNotEmpty()
  @IsString()
  instructions: string;

  @IsNotEmpty()
  @IsUrl()
  tracking_url: string;

  @IsNotEmpty()
  @IsUrl()
  icon: string;

  @IsNotEmpty()
  @Type(() => OS)
  OS: OS;
}
